---
title: "About me"
date: 2023-06-18T22:00:47+02:00
draft: true
---

Faire des projets dans divers domaines, permettre de donner aux étudiants d'apprendre une faculté tout en la pratiquant en ayant des outils adaptés à portée de main, accompagner les élèves pour leur projet tutoré, personnel ou professionnel
